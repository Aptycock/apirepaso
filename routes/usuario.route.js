var express = require('express')
var router = express.Router()
var usuario = require('../model/usuario.model')

router.route('/api/v1/usuario')
    .get(function(req, res){
        usuario.selectAll(function(resultados){
            if(typeof resultados !== undefined) {
                res.json(resultados);
            } else {
                res.json({mensaje: "Done bro"})
            }
        }) 
    })
    .post(function(req, res) {
        res.json({mensaje: "Done bro"})
    })

router.route('/api/v1/usuario/:idUsuario')
    .get(function(req, res){
        res.json({mensaje: "Done bro"})
    })
    .put(function(req, res){
        res.json({mensaje: "Done bro"})
    })
    .delete(function(req, res){
        res.json({mensaje: "Done bro"})
    })

module.exports = router

