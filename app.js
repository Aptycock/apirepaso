var express = require('express');
var bodyParser = require('body-parser');

//Importar rutas
var usuarioRoute = require('./routes/usuario.route')

var app = express()
var port = 3000



//Configuracion BodyParser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.use(function(req, res, next){
    res.setHeader('Acces-Control-Allow-Origin', '*');
})

app.use('/', usuarioRoute)

app.listen(port, function(){
    console.log('the server is fuckin runin bro')
})